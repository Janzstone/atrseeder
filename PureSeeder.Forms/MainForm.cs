﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Deployment.Application;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.Remoting.Contexts;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gecko;
using ATRGamers.ATRSeeder.Core.Annotations;
using ATRGamers.ATRSeeder.Core.Configuration;
using ATRGamers.ATRSeeder.Core.Context;
using ATRGamers.ATRSeeder.Core.Logging;
using ATRGamers.ATRSeeder.Core.Monitoring;
using ATRGamers.ATRSeeder.Core.ProcessControl;
using ATRGamers.ATRSeeder.Core.ServerManagement;
using ATRGamers.ATRSeeder.Core.Settings;
using ATRGamers.ATRSeeder.Forms.Extensions;
using ATRGamers.ATRSeeder.Forms.Properties;
using Timer = System.Windows.Forms.Timer;

namespace ATRGamers.ATRSeeder.Forms
{
    public partial class MainForm : Form
    {
        
        private readonly IDataContext _context;
        private readonly IProcessController _processController;
        private readonly ISeederActionFactory _seederActionFactory;
        //private readonly IdleKickAvoider _idleKickAvoider;
        private readonly ProcessMonitor _processMonitor;
        //private readonly ReadyUpper _readyUpper;
        private readonly Timer _serversRefreshTimer;
        private readonly Timer _browserRefreshTimer;
        private readonly Timer _statusRefreshTimer;
        private readonly Timer _randomSeedTimer;
        private Timer _scheduleSeedTimer;
        private Random _rand;
        private int _randMin = 60*1000;
        private int _randMax = 600*1000;
        private string _version;
        private bool canDoubleClick = false;
        private bool wasSeeding = false;

        // CancellationTokens
        //private CancellationToken _avoidIdleKickCt;
        private CancellationToken _processMonitorCt;
        //private CancellationToken _readyUpdderCt;

        private MainForm()
        {
            InitializeComponent();
        }

        public MainForm(IDataContext context, [NotNull] IProcessController processController,
            [NotNull] ISeederActionFactory seederActionFactory) : this()
        {
            if (context == null) throw new ArgumentNullException("context");
            if (processController == null) throw new ArgumentNullException("processController");
            if (seederActionFactory == null) throw new ArgumentNullException("seederActionFactory");
            _context = context;
            _processController = processController;
            _seederActionFactory = seederActionFactory;

            _context.Session.PropertyChanged += ContextPropertyChanged;
            _context.Settings.PropertyChanged += ContextPropertyChanged;
            
            _browserRefreshTimer = new Timer();
            _statusRefreshTimer = new Timer();
            _serversRefreshTimer = new Timer();
            _rand = new Random();
            _randomSeedTimer = new Timer();

            _processMonitor = _processController.GetProcessMonitor();
            _processMonitor.OnProcessStateChanged += HandleProcessStatusChange;
            //_idleKickAvoider = _processController.GetIdleKickAvoider();
            //_readyUpper = _processController.GetReadyUpper();

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                _version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            else
            {
                _version = "Debugging";
            }
        }

        #region Initialization

        protected async override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            CreateBindings();
            UiSetup();
            
            await RefreshServerStatusesNoSeed();
            

            geckoWebBrowser1.DocumentCompleted += BrowserChanged;

            FirstRunCheck();

            SetupRefreshTimers();

            // Spin up background processes
            SpinUpProcessMonitor();

            _context.ImportSettings("WEBCONFIG", _version);

            //await RefreshServerStatuses();
            updateSettings();

            await LoadBattlelog();

            Logger.Log("Finished initialization.");
        }

        private void UiSetup()
        {
            Icon = Resources.PB;
            notifyIcon1.Icon = Resources.PBOff;
            notifyIcon1.Text = Constants.ApplicationName;
            Text = Constants.ApplicationName;
            closeToolStripMenuItem.Text = String.Format("Close {0}", Constants.ApplicationName);
            SetCurrentSeedingStatusDisplay();
        }

        private async void SpinUpProcessMonitor()
        {
            _processMonitorCt = new CancellationTokenSource().Token;
            await _processMonitor.CheckOnProcess(_processMonitorCt, () => _context.Session.CurrentGame, SynchronizationContext.Current);
        }

        private void FirstRunCheck()
        {
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                if (ApplicationDeployment.CurrentDeployment.IsFirstRun)
                {
                    ShowReleaseNotes();
                }
            }
        }

        private void SetupRefreshTimers()
        {
            SetBrowserRefreshTimer();
            SetStatusRefreshTimer();
            SetServerListRefreshTimer();
            SetRandomSeedTimer();
        }

        private void SetServerListRefreshTimer()
        {
            int serverListRefreshInterval = 300 * 1000;

            _serversRefreshTimer.Interval = serverListRefreshInterval;
            _serversRefreshTimer.Tick += TimedServerListRefresh;
            _serversRefreshTimer.Start();

        }

        private void SetBrowserRefreshTimer()
        {
            int refreshTimerInterval;
            if (!int.TryParse(refreshInterval.Text, out refreshTimerInterval))
                refreshTimerInterval = _context.Settings.RefreshInterval;

            refreshTimerInterval = refreshTimerInterval*1000;

            _browserRefreshTimer.Interval = refreshTimerInterval;

            _browserRefreshTimer.Tick += TimedRefresh;

            _browserRefreshTimer.Start();
        }

        private void SetStatusRefreshTimer()
        {
            int statusRefreshTimerInterval;
            if (!int.TryParse(statusRefreshInterval.Text, out statusRefreshTimerInterval))
                statusRefreshTimerInterval = _context.Settings.StatusRefreshInterval;

            statusRefreshTimerInterval = statusRefreshTimerInterval*1000;

            _statusRefreshTimer.Interval = statusRefreshTimerInterval;

            _statusRefreshTimer.Tick += TimedServerStatusRefresh;

            _statusRefreshTimer.Start();
        }

        private void SetRandomSeedTimer()
        {
            _randomSeedTimer.Tick += RandomSeedTimerHandler;
            
            var timerInterval = _rand.Next(_randMin, _randMax); // Random between 1 and 10 mins
            //SetStatus(String.Format("Time until next seed attempt: {0} seconds", (timerInterval / 1000).ToString()), 5);
            _randomSeedTimer.Interval = timerInterval;
            
        }

        

        private void CreateBindings()
        {
            username.DataBindings.Add("Text", _context.Settings, x => x.Username);
            password.DataBindings.Add("Text", _context.Settings, x => x.Password);
            email.DataBindings.Add("Text", _context.Settings, x => x.Email);

            currentLoggedInUser.DataBindings.Add("Text", _context.Session, x => x.CurrentLoggedInUser);
            //currentGameLabel.DataBindings.Add("Text", _context.Session, x => x.CurrentGameName);

            logging.DataBindings.Add("Checked", _context.Settings, x => x.EnableLogging, false,
                DataSourceUpdateMode.OnPropertyChanged);
            minimizeToTray.DataBindings.Add("Checked", _context.Settings, x => x.MinimizeToTray, false,
                DataSourceUpdateMode.OnPropertyChanged);
            gameBattlefieldFourEnabled.DataBindings.Add("Checked", _context.Settings, x => x.GameBattlefieldFourEnabled, false,
                DataSourceUpdateMode.OnPropertyChanged);
            gameBattlefieldHardlineEnabled.DataBindings.Add("Checked", _context.Settings, x => x.GameBattlefieldHardlineEnabled, false,
                DataSourceUpdateMode.OnPropertyChanged);
            gameBattlefieldThreeEnabled.DataBindings.Add( "Checked", _context.Settings, x => x.GameBattlefieldThreeEnabled, false,
                DataSourceUpdateMode.OnPropertyChanged );
            autoLogin.DataBindings.Add("Checked", _context.Settings, x => x.AutoLogin, false,
                DataSourceUpdateMode.OnPropertyChanged);
            seedingEnabled.DataBindings.Add("Checked", _context.Session, x => x.SeedingEnabled);
            refreshInterval.DataBindings.Add("Text", _context.Settings, x => x.RefreshInterval);
            statusRefreshInterval.DataBindings.Add("Text", _context.Settings, x => x.StatusRefreshInterval);
            autoMinimizeSeeder.DataBindings.Add("Checked", _context.Settings, x => x.AutoMinimizeSeeder, false,
                DataSourceUpdateMode.OnPropertyChanged);
            //autoMinimizeGame.DataBindings.Add("Checked", _context.Settings, x => x.AutoMinimizeGame, false,
            //    DataSourceUpdateMode.OnPropertyChanged);

            checkBoxAutoSeed.DataBindings.Add("Checked", _context.Settings, x => x.AutoStartSeeding, false, 
                DataSourceUpdateMode.OnPropertyChanged);

            checkBoxScheduleSeed.DataBindings.Add("Checked", _context.Settings, x => x.AutoScheduleSeeding, false,
                DataSourceUpdateMode.OnPropertyChanged);

            dateTimePickerStart.DataBindings.Add("Value", _context.Settings, x => x.SeedingStart, false, 
                DataSourceUpdateMode.OnPropertyChanged);

            dateTimePickerEnd.DataBindings.Add("Value", _context.Settings, x => x.SeedingEnd, false,
                DataSourceUpdateMode.OnPropertyChanged);

            saveSettings.DataBindings.Add("Enabled", _context.Settings, x => x.DirtySettings, true,
                DataSourceUpdateMode.OnPropertyChanged);

            var statusBindingSource = new BindingSource() {DataSource = _context.Session.ServerStatuses};
            dataGridView1.DataSource = statusBindingSource;

        }

        #endregion Intialization

        #region EventHandlers

        private void HandleProcessStatusChange(object sender, ProcessStateChangeEventArgs e)
        {
            _context.Session.BfIsRunning = e.IsRunning;


            if ( _context.Session.BfIsRunning && _context.Session.SeedingEnabled ) {
                notifyIcon1.Icon = Resources.PBOn;
            } else {
                notifyIcon1.Icon = Resources.PBOff;
            }

            SetCurrentSeedingStatusDisplay();
        }

        private void SetCurrentSeedingStatusDisplay()
        {
            if ( _context.Session.SeedingEnabled == true ) {
                if ( _context.Session.BfIsRunning ) {
                    currentSeedingStatus.ForeColor = Color.Green;
                    currentSeedingStatus.Text = "Seeding";
                } else {
                    currentSeedingStatus.ForeColor = Color.Red;
                    currentSeedingStatus.Text = "Not Seeding";
                }
            } else {
                if ( _context.Session.BfIsRunning ) {
                    currentSeedingStatus.ForeColor = Color.Orange;
                    currentSeedingStatus.Text = "Playing";
                } else {
                    currentSeedingStatus.ForeColor = Color.Red;
                    currentSeedingStatus.Text = "Not Seeding";
                }
            }
            
        }

        private async void TimedServerListRefresh(object sender, EventArgs e)
        {
            _context.ImportSettings("WEBCONFIG", _version);
            await RefreshServerStatuses();

        }

        private void TimedRefresh(object sender, EventArgs e)
        {
            AnyRefresh();
        }

        private async void TimedServerStatusRefresh(object sender, EventArgs e)
        {
            await RefreshServerStatuses();
        }

        private async void RandomSeedTimerHandler(object sender, EventArgs e)
        {
            // Set the next random tick length
            int timerInterval = _rand.Next(_randMin, _randMax);  // Get a new random interval between 1 and 10 mins
            //SetStatus(String.Format("Time until next seed attempt: {0} seconds", (timerInterval / 1000).ToString()), 5);
            _randomSeedTimer.Interval = timerInterval;

            
            await Seed();
        }

        private async Task RefreshServerStatusesNoSeed()
        {
            await _context.UpdateServerStatuses();
        }

        private async Task RefreshServerStatuses()
        {
            await _context.UpdateServerStatuses();
            //await Seed();  // Using random seeding timer for now RandomSeedTimerHandler(object, EventArgs)
        }

        private async Task Seed()
        {
            var seederAction = await _seederActionFactory.GetAction(_context);
            await SeederActionHandler(seederAction);
        }

        private async Task SeederActionHandler(SeederAction seederAction)
        {
            if (seederAction.ActionType == SeederActionType.Noop)
                return;
            if (seederAction.ActionType == SeederActionType.Stop)
            {
                await _processController.StopGame(_context);
                return;
            }
            if (seederAction.ActionType == SeederActionType.Seed)
            {
                await AttempSeeding(seederAction);
                return;
            }

            throw new ArgumentException("Unknow SeederAction ActionType.");
        }

        private void AnyRefresh()
        {
            DisableRefreshButton();
            RefreshPageAndData();
        }

        private void RefreshPageAndData()
        {
            _browserRefreshTimer.Stop();

            // Create a single use event handler to fire AttemptSeeding after context is updated
            // This is so that only refreshes triggerd by PS will fire Seeding. This will prevent changes
            // made inside the browser (by redirect/javascript/etc.) from firing the Seeding.
            ContextUpdatedHandler handler = null;
            handler = (tSender, tE) =>
            {
                _context.OnContextUpdate -= handler;
                //await RefreshServerStatuses();
            };
            _context.OnContextUpdate += handler;
            RefreshPage();

            _browserRefreshTimer.Start();
        }

        private Task<bool> CanSeed()
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                var loggedIn = _context.Session.CurrentLoggedInUser != Constants.NotLoggedInUsername;
                if (!loggedIn)
                {
                    DialogResult result = MessageBoxEx.Show("User not logged in.", "Cannot Seed",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, 5000);
                }

                return loggedIn && _context.Session.SeedingEnabled;
            });
        }

        private async Task AttempSeeding(SeederAction seederAction)
        {
            if (!await CanSeed())
                return;

            _browserRefreshTimer.Stop();

            ContextUpdatedHandler handler = null;
            handler = (tSender, tE) =>
            {
                _context.OnContextUpdate -= handler;
                _context.Session.CurrentServer = seederAction.ServerStatus;

                //DialogResult result = MessageBoxEx.Show("Seeding in 5 seconds.", "Auto-Seeding", MessageBoxButtons.OKCancel,
                //    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, 5000);
                
                //if (result == DialogResult.Cancel)
                //    return;

                JoinServer();
            };
            _context.OnContextUpdate += handler;

            UpdateGameType(seederAction.ServerStatus);

            await LoadPage(seederAction.ServerStatus.Address);

            _browserRefreshTimer.Start();
        }

        private void UpdateGameType(ServerStatus server)
        {
            string address = server.Address;

            GameInfo BF4 = Constants.Games.BF4;
            GameInfo BFH = Constants.Games.BFH;
            GameInfo BF3 = Constants.Games.BF3;

            if (BF4.UrlMatch.IsMatch(address))
            {
                currentGameLabel.ForeColor = Color.Green;
                currentGameLabel.Text = BF4.GameName;
                _context.Session.CurrentGame = BF4;
            }
            else if (BFH.UrlMatch.IsMatch(address))
            {
                currentGameLabel.ForeColor = Color.Green;
                currentGameLabel.Text = BFH.GameName;
                _context.Session.CurrentGame = BFH;
            } 
            else if ( BF3.UrlMatch.IsMatch( address ) ) {
                currentGameLabel.ForeColor = Color.Green;
                currentGameLabel.Text = BF3.GameName;
                _context.Session.CurrentGame = BF3;
            }
            else
            {
                currentGameLabel.ForeColor = Color.Red;
                currentGameLabel.Text = Constants.Games.Default.GameName;
                _context.Session.CurrentGame = Constants.Games.Default;
            }


        }


        /// <summary>
        /// This method is the event handler for any time the browser is refreshed
        /// </summary>
        private void BrowserChanged(object sender, EventArgs e)
        {
            curUrl.Text = geckoWebBrowser1.Url.ToString();
            UpdateContextWithBrowserData();
        }

        #endregion EventHandlers

        #region BattlelogManipulation

        private void JoinServer()
        {
            // Todo: This should be moved into configuration so it can more easily be changed
            const string jsCommand =
                "document.getElementsByClassName('btn btn-primary btn-large large arrow')[0].click();";
            const string jsCommand2 =
                "document.getElementsByClassName('base-button-arrow-almost-gigantic legacy-server-browser-info-button')[0].click();";
            RunJavascript( jsCommand );
            RunJavascript( jsCommand2 );
            //_processController.MinimizeAfterLaunch();
            _context.JoinServer();
        }

        private async Task LoadBattlelog()
        {
            await LoadPage(Constants.DefaultUrl);
        }

        private async Task LoadPage(string url)
        {
            SetStatus(String.Format("Loading: {0}", url), 3);
            await Navigate(url);
        }

        private void RefreshPage()
        {
            geckoWebBrowser1.Refresh();
        }

        private Task Navigate(string url)
        {
            return Task.Factory.StartNew(() => BeginInvoke(new Action(() => geckoWebBrowser1.Navigate(url))));
        }

        private void ContextPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateInterface();
        }

        /// <summary>
        /// Update the context with the source from the page currently loaded in the browser
        /// </summary>
        private void UpdateContextWithBrowserData()
        {
            var source = string.Empty;
            var pageSource = string.Empty;

            // Get the source for the page currently loaded in the browser
            if (!string.IsNullOrEmpty(geckoWebBrowser1.Document.GetElementsByTagName("html")[0].InnerHtml))
                pageSource = geckoWebBrowser1.Document.GetElementsByTagName("html")[0].InnerHtml;

            source = pageSource;
            _context.UpdateContextWithBrowserPage(source);

            AutoLogin();
        }

        private void UpdateInterface()
        {
            currentLoggedInUser.ForeColor = _context.GetUserStatus() == UserStatus.Correct ? Color.Green : Color.Red;
        }

        private void AutoLogin()
        {
            if (!_context.Settings.AutoLogin)
                return;

            if (_context.GetUserStatus() == UserStatus.Correct)
                return;


            if (_context.GetUserStatus() == UserStatus.Incorrect)
            {
                return;
            }

            Login();
        }

        private async void Login()
        {
            SetStatus("Attempting login.");

            geckoWebBrowser1.Navigate(Constants.DefaultUrl);

            await Sleep( 2 );


            // Todo: This should be moved into configuration so it can more easily be changed
            string jsCommand =
                String.Format(
                    "$('#email').val('{0}'); " +
                    "$('#password').val('{1}');" +
                    "$('#rememberMe').val() == '1';" +
                    "$('#btnLogin').click();",
                    email.Text, password.Text);

            RunJavascript(jsCommand);

            await Sleep(1);
            SetStatus("");
        }

        private void RunJavascript(string javascript)
        {
            using (var context = new AutoJSContext(geckoWebBrowser1.Window.JSContext))
            {
                context.EvaluateScript(javascript);
            }
        }

        private void Logout()
        {
            geckoWebBrowser1.Navigate("http://battlelog.battlefield.com/bf4/session/logout/");
        }

        private Task CheckLogout(Action successfulLogout, Action failedLogout)
        {
            return Task.Factory.StartNew(() =>
            {
                const int logoutCheckCount = 10;
                for (int i = 0; i < logoutCheckCount; i++)
                {
                    if (_context.GetUserStatus() == UserStatus.None)
                    {
                        if (successfulLogout != null)
                            successfulLogout.Invoke();

                        return;
                    }

                    Thread.Sleep(1000);
                }
                if (failedLogout != null)
                    failedLogout.Invoke();
            });
        }

        #endregion BattlelogManipulation

        #region UiEvents

        private void saveSettings_Click(object sender, EventArgs e)
        {
            updateSettings();
        }

        private async void updateSettings()
        {
            _context.ImportSettings("WEBCONFIG", _version);
            await RefreshServerStatuses();
            _context.Settings.SaveSettings();
        }

        private void joinServerButton_Click(object sender, EventArgs e)
        {
            startSeeding();
        }

        private void stopSeedingButton_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Seeding has been disabled. Do you want to close Battlefield?", "Close Battlefield?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            bool killGame = ( confirmResult == DialogResult.Yes );
            
            stopSeeding(killGame);

        }

        private void killGameButton_Click( object sender, EventArgs e ) {
            var confirmResult = MessageBox.Show( "Are you sure you want to close Battlefield?", "Close Battlefield", MessageBoxButtons.YesNo );
            if ( confirmResult == DialogResult.Yes ) {
                this.killGame();
            }
        }

        private void stopSeeding() {
            stopSeeding( true );
        }

        private async void stopSeeding(bool killGame)
        {
            if ( _context.Session.BfIsRunning ) {
                this.wasSeeding = true;
            }
            // this.wasSeeding = true;
            this.seedingEnabled.Checked = false;
            this.startSeedingButton.Enabled = true;
            this.stopSeedingButton.Enabled = false;
            _randomSeedTimer.Enabled = false;
            _randomSeedTimer.Stop();
            _context.Settings.SaveSettings();
            if ( killGame == true ) {
                this.killGame();
            } else {
                this.killGameButton.Enabled = true;
            }
            SetCurrentSeedingStatusDisplay();
            await RefreshServerStatuses();
        }

        private async void startSeeding()
        {
            this.seedingEnabled.Checked = true;
            this.stopSeedingButton.Enabled = true;
            this.startSeedingButton.Enabled = false;
            this.killGameButton.Enabled = false;
            if ( _context.Session.BfIsRunning ) {
                if ( this.wasSeeding == true ) {
                    var response = MessageBox.Show( "Battlefield must be stopped in order to start seeding.\nAre you sure you want to start seeding?", "Start Seeding?", MessageBoxButtons.YesNo, MessageBoxIcon.Question );
                    if ( response == DialogResult.Yes ) {
                        killGame();
                        await Sleep( 5 );
                    } else {
                        return;
                    }

                }
            } else {
                this.wasSeeding = false;
            }
            _randomSeedTimer.Enabled = true;
            _randomSeedTimer.Start();
            _context.Settings.SaveSettings();
            await RefreshServerStatuses();
            await Seed();
        }

        private async void killGame() {
            this.killGameButton.Enabled = false;
            this.wasSeeding = false;
            await _processController.StopGame( _context );
        }

        private void geckoWebBrowser1_DomContentChanged(object sender, DomEventArgs e)
        {
            BrowserChanged(sender, e);
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            AnyRefresh();
        }

        private void showWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void minimizeWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState && _context.Settings.MinimizeToTray)
            {
                Hide();
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveSettingsDialog.ShowDialog();
        }

        private void saveSettingsDialog_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = saveSettingsDialog.FileName;
            _context.ExportSettings(fileName);
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            importSettingsDialog.ShowDialog();
        }

        private void importSettingsDialog_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = importSettingsDialog.FileName;
            _context.ImportSettings(fileName);
            AnyRefresh();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Show();
                WindowState = FormWindowState.Normal;
            }
            else
            {
                Hide();
                WindowState = FormWindowState.Minimized;
            }
        }

        private void MainForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible == false)
            {
                notifyIcon1.ShowBalloonTip(500, "ATRSeeder Still Running",
                    "Right click or double click to restore the window", ToolTipIcon.Info);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void viewReleaseNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutDialog().ShowDialog();
        }

        private async void statusRefresh_Click(object sender, EventArgs e)
        {
            await RefreshServerStatuses();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            //dataGridView1.ClearSelection();
        }

        private async void dataGridView1_CellDoubleClicked( object sender, DataGridViewCellEventArgs e ) {

            if ( canDoubleClick == false ) {
                MessageBox.Show( "Application Initializing. Please wait for your username to show up before attempting to join a server." );
            } else {

                var response = DialogResult.No;
                var row = dataGridView1.SelectedRows[0];
                
                string URL = row.Cells[3].Value.ToString();
                string ServerName = row.Cells[2].Value.ToString();
                if ( this.seedingEnabled.Checked == true ) {
                    response = MessageBox.Show( "Are you sure you want to join:\n" + ServerName + "?\nThis will override seeding for the current server, you will be moved again when the next server needs to be seeded.", "Join Server?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
                } else {
                    response = DialogResult.Yes;
                }
                
                if ( response == DialogResult.Yes ) {
                    this.killGameButton.Enabled = true;
                    await LoadPage( URL );
                    await Sleep( 5 );
                    JoinServer();
                }
            }
        }

        #endregion UiEvents

        #region UiManipulation

        private async void DisableRefreshButton()
        {
            refresh.Enabled = false;
            await Sleep(5);
            refresh.Enabled = true;
        }


        private async void SetStatus(string status, int time = -1)
        {
            toolStripStatusLabel1.Text = status;
            statusStrip1.Refresh();

            if (time > -1)
            {
                await Sleep(time);
                SetStatus("");
            }
        }

        private static void ShowReleaseNotes()
        {
            new ReleaseNotes().ShowDialog();
        }

        #endregion UiManipulation

        private Task Sleep(int seconds)
        {
            return Task.Factory.StartNew(() => Thread.Sleep(seconds*1000));
        }

        private void currentLoggedInUser_TextChange(object sender, EventArgs e)
        {
            if (currentLoggedInUser.Text == Constants.NotLoggedInUsername || currentLoggedInUser.Text == "")
            {
                stopSeeding();
                canDoubleClick = false;
                startSeedingButton.Enabled = false;
                currentLoggedInUser.ForeColor = Color.Red;
            }
            else
            {
                canDoubleClick = true;
                startSeedingButton.Enabled = true;
                currentLoggedInUser.ForeColor = Color.Green;

                // Start seeding if we've got auto seeding, or we've just started and it's passed the time of day to seed.

                if (checkBoxAutoSeed.Checked || (checkBoxScheduleSeed.Checked && (DateTime.Now.TimeOfDay > _context.Settings.SeedingStart.TimeOfDay && DateTime.Now.TimeOfDay < _context.Settings.SeedingEnd.TimeOfDay)))
                {
                    startSeeding();
                }
            }
        }

        // This is just to tidy up the interface slightly...

        private bool isMoved = false;

        private void UserSettingsTab_Paint(object sender, PaintEventArgs e)
        {

            if (!isMoved)
            {
                isMoved = true;
                // Shift up controls if the Username isn't visible...  
                foreach (Control ctl in UserSettingsTab.Controls)
                {
                    if (ctl.Visible == true)
                    {
                        ctl.Location = new Point(ctl.Location.X, ctl.Location.Y - 27);
                    }
                }
            }
        }

        private void checkBoxAutoSeed_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBoxAutoSeed.Checked)
            {
                checkBoxScheduleSeed.Checked = false;
                autoLogin.Checked = true;
                
            }
            autoLogin.Enabled = !checkBoxAutoSeed.Checked;
            checkBoxScheduleSeed.Enabled = !checkBoxAutoSeed.Checked;
            dateTimePickerStart.Enabled = !checkBoxAutoSeed.Checked;
            dateTimePickerEnd.Enabled = !checkBoxAutoSeed.Checked;

        }

        private void checkBoxScheduleSeed_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBoxScheduleSeed.Checked)
            {
                _scheduleSeedTimer = new Timer();

                _scheduleSeedTimer.Interval = 1000 * 30;
                _scheduleSeedTimer.Tick += TimedScheduleSeedRefresh;
                _scheduleSeedTimer.Start();


                checkBoxAutoSeed.Enabled = false;
                checkBoxAutoSeed.Checked = false;
                autoLogin.Checked = true;
            }
            else
            {
                checkBoxAutoSeed.Enabled = true;
            }
            autoLogin.Enabled = !checkBoxScheduleSeed.Checked;

        }

        private void TimedScheduleSeedRefresh(object sender, EventArgs e)
        {
           
            if (DateTime.Now.Hour == _context.Settings.SeedingStart.Hour && DateTime.Now.Minute == _context.Settings.SeedingStart.Minute)
            {
                if (currentSeedingStatus.Text == "Seeding")
                    return;
                startSeeding();
            }

            if (DateTime.Now.Hour == _context.Settings.SeedingEnd.Hour && DateTime.Now.Minute == _context.Settings.SeedingEnd.Minute)
            {
                if (currentSeedingStatus.Text == "Not Seeding")
                    return;
                stopSeeding();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            // Upgrade?
            if (Properties.Settings.Default.WindowSize.Width == 0) Properties.Settings.Default.Upgrade();

            // if they hold the shift key, don't restore the window size.

            if ((ModifierKeys & Keys.Shift) == 0)
            {
                if (Properties.Settings.Default.WindowSize.Width == 0 || Properties.Settings.Default.WindowSize.Height == 0)
                {
                    // first start
                    // optional: add default values
                }
                else
                {
                    this.WindowState = Properties.Settings.Default.WindowState;

                    // we don't want a minimized window at startup
                    /*if (this.WindowState == FormWindowState.Minimized)
                        this.WindowState = FormWindowState.Normal;*/

                    this.Location = Properties.Settings.Default.WindowLocation;
                    this.Size = Properties.Settings.Default.WindowSize;
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.WindowState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
            {
                // save location and size if the state is normal
                Properties.Settings.Default.WindowLocation = this.Location;
                Properties.Settings.Default.WindowSize = this.Size;
            }
            else
            {
                // save the RestoreBounds if the form is minimized or maximized!
                Properties.Settings.Default.WindowLocation = this.RestoreBounds.Location;
                Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            }

            // don't forget to save the settings
            Properties.Settings.Default.Save();
        }
    }
}